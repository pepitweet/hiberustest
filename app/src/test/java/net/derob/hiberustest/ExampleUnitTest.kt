package net.derob.hiberustest

import kotlinx.coroutines.runBlocking
import net.derob.hiberustest.ws.RetrofitHelper
import org.junit.Assert.assertEquals
import org.junit.Test
import java.net.HttpURLConnection

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun testGetCards() {
        runBlocking {
            val response = RetrofitHelper.getCards()
            if (response.statusCode == HttpURLConnection.HTTP_OK && !response.objeto.isNullOrEmpty()) {
                val list = response.objeto!!
                for (item in list.sortedWith(compareBy { it.name})) {
                    println("Card: ${item.name}")
                }
            }
        }
    }

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
