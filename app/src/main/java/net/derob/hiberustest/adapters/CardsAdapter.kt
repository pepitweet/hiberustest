package net.derob.hiberustest.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.custom_item_card.view.*
import net.derob.hiberustest.R
import net.derob.hiberustest.commons.Const.BASE_IMAGES_URL
import net.derob.hiberustest.models.Card

class CardsAdapter(
    private val adapterContext: Context,
    private val items: List<Card>
) : RecyclerView.Adapter<CardsAdapter.CardsViewHolder>() {
    private var selectListener: (Card)  -> Unit = {}

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CardsViewHolder {
        return CardsViewHolder(
            LayoutInflater
                .from(adapterContext)
                .inflate(R.layout.custom_item_card, parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: CardsViewHolder,
        position: Int
    ) {
        val card = items[position]
        holder.tvNameCard?.text = card.name
        holder.cvItem?.setOnClickListener {
            selectListener(card)
        }
        holder.ivCard?.let {iv ->
            if (card.drawableImage != null) {
                iv.setImageDrawable(card.drawableImage)
                return
            }

            Glide
                .with(adapterContext)
                .load(String.format(BASE_IMAGES_URL, card.idName))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?,
                                              isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?,
                        dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        resource?.let {
                            card.drawableImage = it
                            iv.setImageDrawable(it)
                        }
                        return false
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv)
        }
    }

    fun setOnSelectListener(listener: (Card) -> Unit) {
        selectListener = listener
    }

    class CardsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cvItem: ConstraintLayout? = view.cvItem
        val ivCard: AppCompatImageView? = view.ivCard
        val tvNameCard: AppCompatTextView? = view.tvNameCard
    }
}