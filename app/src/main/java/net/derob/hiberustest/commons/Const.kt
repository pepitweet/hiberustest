package net.derob.hiberustest.commons

object Const {
    const val BASE_URL = "http://www.clashapi.xyz/"
    const val BASE_IMAGES_URL = "${BASE_URL}images/cards/%s.png"

    const val CARD = "card"

    object WebService {
        const val GET_CARDS = "api/cards"
    }

    object Params {
        const val ID = "_id"
        const val ID_NAME = "idName"
        const val RARITY = "rarity"
        const val TYPE = "type"
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val ELIXIR_COST = "elixirCost"
    }
}