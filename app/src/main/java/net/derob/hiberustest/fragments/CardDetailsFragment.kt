package net.derob.hiberustest.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_card_details.*
import net.derob.hiberustest.R
import net.derob.hiberustest.commons.Const.CARD
import net.derob.hiberustest.models.Card
import net.derob.hiberustest.viewModels.BaseViewModel

class CardDetailsFragment : BaseFragment() {

    override fun upcastBaseViewModel(): BaseViewModel? {
        return null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_card_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val card = arguments?.getSerializable(CARD) as Card?
        if (card != null) {
            tvCardName?.text = card.name
            tvDescription?.text = card.description
            ivCard?.visibility = View.VISIBLE
            ivCard?.setImageDrawable(card.drawableImage)
            tvRarityType?.text = getString(R.string.rarity_type_card, card.rarity, card.type)
        } else {
            tvCardName?.text = ""
            tvDescription?.text = ""
            tvRarityType?.text = ""
            ivCard?.visibility = View.GONE
        }
    }
}