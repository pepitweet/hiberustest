package net.derob.hiberustest.fragments

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import net.derob.hiberustest.R
import net.derob.hiberustest.viewModels.BaseViewModel

class MasterDetailFragment : BaseFragment() {

    override fun upcastBaseViewModel(): BaseViewModel? {
        return null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_master_detail_host, container, false)

        val master = childFragmentManager.findFragmentById(R.id.master_nav_fragment) as NavHostFragment?
        if (master != null) {
            val navController = master.navController
            val navInflater = navController.navInflater
            val graph = navInflater.inflate(R.navigation.master)

            master.navController.setGraph(graph, arguments)
        }

        val detail = childFragmentManager.findFragmentById(R.id.detail_nav_fragment) as NavHostFragment?
        if(detail != null) {
            val navController = detail.navController
            val navInflater = navController.navInflater
            val graph = navInflater.inflate(R.navigation.detail)

            detail.navController.setGraph(graph, arguments)
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        mainActivity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }
}