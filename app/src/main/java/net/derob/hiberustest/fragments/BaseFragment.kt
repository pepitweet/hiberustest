package net.derob.hiberustest.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import net.derob.hiberustest.MainActivity
import net.derob.hiberustest.R
import net.derob.hiberustest.models.RequestResponse
import net.derob.hiberustest.viewModels.BaseViewModel

abstract class BaseFragment : Fragment() {
    lateinit var mainActivity: MainActivity
    protected lateinit var baseViewModel: BaseViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity)
            mainActivity = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModelHolder = upcastBaseViewModel()
        baseViewModel = viewModelHolder ?: ViewModelProviders.of(this).get(BaseViewModel::class.java)
        mainActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mainActivity.supportActionBar?.setHomeButtonEnabled(true)

        baseViewModel.isLoading.observe(this, Observer { isLoading ->
            isLoading ?: return@Observer

            if (isLoading)
                mainActivity.showLoading()
            else
                mainActivity.hideLoading()
        })

        baseViewModel.errorMessage.observe(this, Observer {
            mainActivity.hideLoading()
            when (it) {
                is String -> mainActivity.showErrorMsg(getString(R.string.error_throw, it))
                is Int -> mainActivity.showErrorMsg(getString(it))
                is RequestResponse<*> -> mainActivity.showErrorMsg(getString(R.string.error_request, it.statusCode.toString()))
            }
        })
    }

    protected abstract fun upcastBaseViewModel(): BaseViewModel?
}