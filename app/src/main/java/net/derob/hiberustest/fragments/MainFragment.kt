package net.derob.hiberustest.fragments

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_main.*
import net.derob.hiberustest.R
import net.derob.hiberustest.viewModels.BaseViewModel

class MainFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnStart.setOnClickListener {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                findNavController().navigate(R.id.action_mainFragment_to_masterDetailFragment)
            else
                findNavController().navigate(R.id.action_mainFragment_to_cardsFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        mainActivity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mainActivity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
    }

    override fun upcastBaseViewModel(): BaseViewModel? {
        return null
    }
}