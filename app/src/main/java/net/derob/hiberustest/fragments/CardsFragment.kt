package net.derob.hiberustest.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_cards.*
import net.derob.hiberustest.R
import net.derob.hiberustest.adapters.CardsAdapter
import net.derob.hiberustest.commons.Const.CARD
import net.derob.hiberustest.viewModels.BaseViewModel
import net.derob.hiberustest.viewModels.CardsViewModel

class CardsFragment : BaseFragment() {
    private lateinit var viewModel: CardsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = baseViewModel as CardsViewModel

        tiedBusqueda.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                Toast.makeText(mainActivity, "Buscar!!!", Toast.LENGTH_SHORT).show()
                viewModel.getItems(textView.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        viewModel.getItems("")

        viewModel.items.observe(this, Observer { cards->
            Log.d("TAG", "cards count: ${cards.size}")

            val rvContext = rvCards.context
            val laController = AnimationUtils.loadLayoutAnimation(rvContext, R.anim.layout_fall_down)

            val adapter = CardsAdapter(mainActivity, cards)
            adapter.setOnSelectListener {card ->
                val bundle = bundleOf(CARD to card)
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
                    swapDetailFragment(bundle)
                } else
                    findNavController().navigate(R.id.action_cardsFragment_to_cardDetailsFragment, bundle)
            }
            rvCards.adapter = adapter
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                rvCards.layoutManager = LinearLayoutManager(mainActivity)
            else
                rvCards.layoutManager = GridLayoutManager(mainActivity, 3)

            rvCards.layoutAnimation = laController
            rvCards.adapter?.notifyDataSetChanged()
            rvCards.scheduleLayoutAnimation()
        })
    }

    override fun upcastBaseViewModel(): BaseViewModel? {
        return ViewModelProviders.of(this).get(CardsViewModel::class.java)
    }

    private fun swapDetailFragment(args: Bundle) {
        parentFragment?.let {
            val detail = it.fragmentManager?.findFragmentById(R.id.detail_nav_fragment) as NavHostFragment?
            if (detail != null) {
                val navController = detail.navController
                val navInflater = navController.navInflater
                val graph = navInflater.inflate(R.navigation.detail)

                detail.navController.setGraph(graph, args)

            }
        }
    }
}