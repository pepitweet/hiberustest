package net.derob.hiberustest

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.rakezbohara.gifdialog.GifDialog
import libs.mjn.prettydialog.PrettyDialog

private const val GIF_TAG = "LOADING_TAG"

class MainActivity : AppCompatActivity() {
    private lateinit var dialogLoading: GifDialog
    private lateinit var pDialog: PrettyDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dialogLoading = GifDialog.with(this)
        pDialog = PrettyDialog(this)
        pDialog.setIcon(R.drawable.pdlg_icon_info, R.color.pdlg_color_gray) {}
        pDialog.addButton(getString(R.string.ok_button), R.color.pdlg_color_white, R.color.pdlg_color_gray) {
            pDialog.dismiss()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showLoading() {
        dialogLoading
            .isCancelable(false)
            .setResourceId(R.drawable.loading)
            .setHeight(300)
            .setWidth(200)
        dialogLoading.showDialog(GIF_TAG)
    }

    fun hideLoading() {
        dialogLoading.dismissDialog(GIF_TAG)
    }

    fun showErrorMsg(msg: String) {
        if (pDialog.isShowing)
            pDialog.dismiss()

        pDialog.setMessage(msg)
        pDialog.show()
    }

    fun dissmisErrorMsg() {
        if (pDialog.isShowing)
            pDialog.dismiss()
    }

    override fun onPause() {
        super.onPause()
        dissmisErrorMsg()
        hideLoading()
    }

    override fun onStop() {
        super.onStop()
        dissmisErrorMsg()
        hideLoading()
    }

    override fun onDestroy() {
        super.onDestroy()
        dissmisErrorMsg()
        hideLoading()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        dissmisErrorMsg()
        hideLoading()
    }
}
