package net.derob.hiberustest.models

class RequestResponse<T> {
    var objeto: T? = null
    var statusCode = 0
    var throwable: Throwable? = null
}