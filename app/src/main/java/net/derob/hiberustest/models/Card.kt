package net.derob.hiberustest.models

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName
import net.derob.hiberustest.commons.Const.Params.DESCRIPTION
import net.derob.hiberustest.commons.Const.Params.ELIXIR_COST
import net.derob.hiberustest.commons.Const.Params.ID
import net.derob.hiberustest.commons.Const.Params.ID_NAME
import net.derob.hiberustest.commons.Const.Params.NAME
import net.derob.hiberustest.commons.Const.Params.RARITY
import net.derob.hiberustest.commons.Const.Params.TYPE
import java.io.Serializable

data class Card(
    @SerializedName(ID)
    val id: String?,
    @SerializedName(ID_NAME)
    val idName: String?,
    @SerializedName(RARITY)
    val rarity: String?,
    @SerializedName(TYPE)
    val type: String?,
    @SerializedName(NAME)
    val name: String?,
    @SerializedName(DESCRIPTION)
    val description: String?,
    @SerializedName(ELIXIR_COST)
    val  elixirCost: Int?
) : Serializable {
    var drawableImage: Drawable? = null
}