package net.derob.hiberustest.ws

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import net.derob.hiberustest.commons.Const.BASE_URL
import net.derob.hiberustest.models.Card
import net.derob.hiberustest.models.RequestResponse
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

object RetrofitHelper {

    private val client = OkHttpClient.Builder()
        .callTimeout(10, TimeUnit.SECONDS)
        .connectTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build()

    private val gsonBuilder = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(
            GsonConverterFactory.create(gsonBuilder)
        ).build()

    private val restClient = retrofit.create(RestClient::class.java)

    suspend fun getCards(): RequestResponse<List<Card>> {
        val call = restClient.getCards()
        return performRequest(call)
    }

    private suspend fun <T>performRequest(call: Call<T>) = suspendCoroutine<RequestResponse<T>> { continuation ->
        val result = RequestResponse<T>()

        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                result.objeto = response.body()
                result.statusCode = response.code()
                continuation.resume(result)
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                result.throwable = t
                continuation.resume(result)
            }
        })
    }
}