package net.derob.hiberustest.ws

import net.derob.hiberustest.commons.Const
import net.derob.hiberustest.models.Card
import retrofit2.Call
import retrofit2.http.GET

interface RestClient {
    @GET(Const.WebService.GET_CARDS)
    fun getCards(): Call<List<Card>>
}