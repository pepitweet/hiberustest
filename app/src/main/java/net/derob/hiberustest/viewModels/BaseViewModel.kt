package net.derob.hiberustest.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    var isLoading = MutableLiveData<Boolean>()
    var errorMessage = MutableLiveData<Any>()
}