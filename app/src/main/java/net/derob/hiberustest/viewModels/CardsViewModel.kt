package net.derob.hiberustest.viewModels

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.derob.hiberustest.R
import net.derob.hiberustest.commons.DataSession
import net.derob.hiberustest.models.Card
import net.derob.hiberustest.ws.RetrofitHelper
import java.net.HttpURLConnection

class CardsViewModel : BaseViewModel() {
    val items = MutableLiveData<List<Card>>()

    fun getItems(toSearch: String) {
        isLoading.postValue(true)
        if (DataSession.cardList.isNotEmpty()) {
            if (toSearch.isNullOrEmpty())
                items.postValue(DataSession.cardList)
            else
                searchInfo(toSearch)

            isLoading.postValue(false)
        } else {
            GlobalScope.launch {
                val response = RetrofitHelper.getCards()

                if (response.statusCode != HttpURLConnection.HTTP_OK || response.objeto.isNullOrEmpty()) {
                    when {
                        response.throwable != null -> errorMessage.postValue(response.throwable!!.message)
                        response.statusCode <= 0 -> errorMessage.postValue(R.string.unexpected_error)
                        else -> errorMessage.postValue(response)
                    }
                    return@launch
                }
                DataSession.cardList = response.objeto!!.sortedWith(compareBy { it.name})
                if (toSearch.isNullOrEmpty())
                    items.postValue(DataSession.cardList)
                else
                    searchInfo(toSearch)

                isLoading.postValue(false)
            }
        }
    }

    private fun searchInfo(toSearch: String) {
        val resultList = DataSession.cardList.filter {
                card -> !card.name.isNullOrBlank() && card.name.contains(toSearch, true)
        }
        items.postValue(resultList)
    }
}