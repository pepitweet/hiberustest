# HiberusTest
Aplicación creada para la resolución del Test enviado. El cual consulta 
un API de cartas del Clash Royal y muestra sus detalles. Utilizando 
Fragments principalmente, ViewModels Navigation Components.

**Importante:** Para habilitar la vista Master/Detail, se debe poner en 
modo landscape desde la pantalla principal.

## Librerias
**Glide** Para imágenes
**Retrofit** Para consumo del API REST
**GifDialog** Para indicador de espera
**PrettyDialog** Para mostrar dialogo con errores de conexión


## Tests
Se realiza un pequeño unitTest para probar la descarga de la información

Se realiza un pequeño UI Test para probar el flujo **Importante** solo 
funciona en modo portrait

por **José Roberto Trejo Hernández**